#include <stdio.h>
#include <iostream>
#include <ctime>
#include <cstdlib>
#include <math.h>
#include <set>
#include <random>
#include <queue>
#include <algorithm>
#include <map>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

using namespace std;

class Edge{
	private:
		int vertexA;
		int vertexB;
	public:
		Edge(){
			vertexA = -1;
			vertexB = -1;
		}
		Edge(int a, int b){
			vertexA = a;
			vertexB = b;
		}
		void printEdge(){
			cout << "(" << vertexA << ", " << vertexB << "), " << "  ";
		}
		bool operator<(const Edge& other) const{
			if (vertexA != other.vertexA) {
				return vertexA < other.vertexA;
			}
			else {
				return vertexB < other.vertexB;		
			}
		}
		int getVertexA(){
			return vertexA;
		}
		int getVertexB(){
			return vertexB;
		}
};

class BipartiteGraphStream
{
	private:
		int nrOfNodes;
		vector<int> A;
		set<int> ASet;
		vector<int> B;
		set<int> BSet;
		vector<int> Bcopy;
		vector<Edge> edges;
		int Asize;
		int Bsize;
		int Edgesize;
		vector<Edge>::const_iterator streamIterator = edges.begin(); 
	public:
		BipartiteGraphStream(int nodes){
			nrOfNodes = nodes;
			random_device rd;
			mt19937 gen(1233214);
			uniform_real_distribution<> dis(0, 1);
			for(int i = 0; i < nrOfNodes; i++){
				float res = dis(gen);
				if (res < 0.5) {
					A.push_back(i);
					ASet.insert(i);
				}
				else {
					B.push_back(i);
					BSet.insert(i);
				}
			}
			Asize = A.size();
			Bsize = B.size();
			
			//add edges
			random_device rd2;
			mt19937 gen2(rd2());
			uniform_int_distribution<> dis2(1, (Bsize-1)/5);
			uniform_int_distribution<> disForA(0, (Asize-1));

			int res;
			int aValue;
			vector<int> Bcopy;
			Bcopy.reserve(B.size());
			copy(B.begin(),B.end(),back_inserter(Bcopy));
			int nrOfEdges = 0;
			for (typename vector<int>::const_iterator i = A.begin(); i != A.end(); ++i) {
				aValue = (int)*i;
				res = dis2(gen2);
				random_shuffle(B.begin(), B.end());
				for (int j = 0; j < res; j++) {
					Edge edge = Edge(aValue, B.at(j));
					if (find(Bcopy.begin(), Bcopy.end(), B.at(j)) != Bcopy.end()) {
						Bcopy.erase(find(Bcopy.begin(), Bcopy.end(), B.at(j)));
					}
					
					edges.push_back(edge);
					nrOfEdges++;
				}
			}
			cout <<  "Number of edges in graph " << nrOfEdges << endl;
			if (Bcopy.size() == 0) {
				for (typename vector<int>::const_iterator i = Bcopy.begin(); i != Bcopy.end(); ++i) {
					Edge e = Edge(disForA(gen2), *i);
					edges.push_back(e);
				}
			}			
		}
		
		void initStream(){
			streamIterator = edges.begin();
		}
		
		bool hasNext(){
			return (streamIterator != edges.end());
		}
		
		Edge getNext(){
			Edge e = (Edge)*streamIterator;
			++streamIterator;
			return e;
		}
		
		vector<int> getA(){
			return A;
		}
		
		set<int> getASet(){
			return ASet;
		}
		
		vector<int> getB(){
			return B;
		}
		
		set<int> getBSet(){
			return BSet;
		}
		
		int numberOfNodes(){
			return nrOfNodes;
		}
		
};

set<Edge> inclusionMaximalMatching(BipartiteGraphStream bgs){
	set<int> visited;
	set<Edge> incMatrixMatching;
	bgs.initStream();
	while(bgs.hasNext()){
		Edge e = bgs.getNext();
		if (visited.find(e.getVertexA()) == visited.end() && visited.find(e.getVertexB()) == visited.end()) {
			visited.insert(e.getVertexA());
			visited.insert(e.getVertexB());
			incMatrixMatching.insert(e);
		}
	}
	return incMatrixMatching;
}

bool checkMatching(set<Edge> M){
	set<int> verticesInM;
	for (set<Edge>::iterator it = M.begin(); it != M.end(); ++it){
		Edge e = (Edge)*it;
		if (verticesInM.find(e.getVertexA()) == verticesInM.end() && verticesInM.find(e.getVertexB()) == verticesInM.end()) {
			verticesInM.insert(e.getVertexA());
			verticesInM.insert(e.getVertexB());
		}else{
			return false;
		}
	}
	return true;
}

set<int> getFree(BipartiteGraphStream bgs, set<Edge> M, bool A){
	set<int> vertices;
	if(A){
		vertices = bgs.getASet();
	}else{
		vertices = bgs.getBSet();
	}
	for (set<Edge>::iterator it = M.begin(); it != M.end(); ++it){
		if(A){
			if(vertices.find(((Edge)*it).getVertexA()) != vertices.end()){
				vertices.erase(((Edge)*it).getVertexA());
			}
		}else{
			if(vertices.find(((Edge)*it).getVertexB()) != vertices.end()){
				vertices.erase(((Edge)*it).getVertexB());
			}
		}
	}
	return vertices;
}

vector<int> getPathEndingWithA(map<int,vector<int>> I, int a){
	for(auto it = I.begin(); it != I.end(); it++){
		vector<int> path = it->second;
		int lastElement = path.back();
		if(lastElement == a){
			return path;
		}
	}
	return vector<int>();
} 

set<Edge> makePathMapToEdgeSet(map<int,vector<int>> A){
	set<Edge> disjointPathEdgeSet;
	for(auto it = A.begin(); it != A.end(); it++){
		vector<int> path = it->second;
		bool fromAToB = true;
		int previous = *path.begin();
		for (auto itVec = ++path.begin(); itVec != path.end(); ++itVec){
			int current = *itVec;
			if(fromAToB){
				fromAToB = false;
				Edge e = Edge(previous, current);
				disjointPathEdgeSet.insert(e);
			}else{
				fromAToB = true;
				Edge e = Edge(current,previous);
				disjointPathEdgeSet.insert(e);
			}
			previous = current;
		}
	}
	disjointPathEdgeSet.size();
	return disjointPathEdgeSet;
}

pair<int,set<Edge>> disjointPaths(BipartiteGraphStream bgs, float threshold, int k, set<Edge> M){
	map<Edge,int> limits;
	map<int,Edge> edgeMap;
	//line 1 in algorithm 2
	for (set<Edge>::iterator it = M.begin(); it != M.end(); ++it){
		Edge e = *it;
		limits[e] = k + 1;
		edgeMap[e.getVertexB()] = e;
	}
	//line 2, 3 in algorithm 2
	map<int,vector<int>> P;
	map<int,vector<int>> I;
	map<int,vector<int>> A;
	//line 4 in algorithm 2
	set<int> Vprime;
	for(int i = 0; i < bgs.numberOfNodes(); i++){
		Vprime.insert(i);
	}
	//line 5-7 in algorithm 2
	set<int> AFree = getFree(bgs, M, true);
	set<int> BFree = getFree(bgs, M, false);
	for (set<int>::iterator it = AFree.begin(); it != AFree.end(); ++it){
		vector<int> path;
		path.push_back(*it);
		P[*it] = path;
		I[*it] = path;
	}
	//line 8 in algorithm 2
	while(true){
		map<int,bool> stuck;
		//line 9 in algorithm 2
		for (set<int>::iterator it = AFree.begin(); it != AFree.end(); ++it){
			stuck[*it] = true;
		}
		bgs.initStream();
		//line 10 in algorithm 2
		while(bgs.hasNext()){
			Edge e = bgs.getNext();
			int a = e.getVertexA();
			int b = e.getVertexB();
			//line 11 in algorithm 2
			if(Vprime.find(a) != Vprime.end() and Vprime.find(b) != Vprime.end()){
				vector<int> pathEndingWithA = getPathEndingWithA(I, a);
				if(pathEndingWithA.size() > 0){
					//line 12 in algorithm 2
					int i = (pathEndingWithA.size()-1)/2+1;
					//line 13 in algorithm 2
					if(BFree.find(b) != BFree.end()){
						pathEndingWithA.push_back(b);
						//line 14 in algorithm 2
						P[pathEndingWithA.front()] = pathEndingWithA;
						//line 15 in algorithm 2
						A[pathEndingWithA.front()] = pathEndingWithA;
						//line 16 in algorithm 2
						I.erase(I.find(pathEndingWithA.front()));
						//line 17 in algorithm  2
						for (vector<int>::iterator it = pathEndingWithA.begin(); it != pathEndingWithA.end(); ++it){
							set<int>::iterator itSet = Vprime.find(*it);
							if(itSet != Vprime.end()){
								Vprime.erase(itSet);
							}
						}
					}
					//line 18-29 in pdf
					else if(edgeMap.find(b) != edgeMap.end()){
						Edge m = edgeMap.find(b)->second;
						int mLimit = limits.find(m)->second;
						if(i < mLimit){
							bool bIsInSomeIncompletePath = false;
							for(auto it = I.begin(); it != I.end(); it++){
								vector<int> path = it->second;
								auto bPositionIterator = find(path.begin(), path.end(), b);
								if(bPositionIterator != path.end()){
									bIsInSomeIncompletePath = true;
									//line 24-29 in pdf
									for (auto it = bPositionIterator; it != path.end(); ++it){
										pathEndingWithA.push_back(*it);
										Edge laterStartingB = edgeMap.find(*it)->second;
										if(edgeMap.find(*it) != edgeMap.end()){
												limits[laterStartingB] = i;
												i++;
										}
									}
									path.erase(bPositionIterator, path.end());
									I[path.front()] = path;
									P[path.front()] = path;
									I[pathEndingWithA.front()] = pathEndingWithA;
									P[pathEndingWithA.front()] = pathEndingWithA;
									stuck[path.front()] = false;
									stuck[pathEndingWithA.front()] = false;
									break; 
								}
							}
							if(!bIsInSomeIncompletePath){
								//line 20-22 in pdf
								pathEndingWithA.push_back(b);
								pathEndingWithA.push_back(m.getVertexA());
								P[pathEndingWithA.front()] = pathEndingWithA;
								I[pathEndingWithA.front()] = pathEndingWithA;
								stuck[pathEndingWithA.front()] = false;
								limits[m] = i;
							}
						}
					}
				}
			}
		}
		int sizeOfI = 0;
		for(auto it = I.begin(); it != I.end(); it++){
			vector<int> path = it->second;
			if(path.size() > 1){
				sizeOfI++;
			}
		}
		//line 30 in algorithm 2
		if(sizeOfI <= M.size()*threshold){
			break;
		}
		//line 31,32 in algorithm 2
		for(auto it = I.begin(); it != I.end(); it++){
			vector<int> path = it->second;
			if(path.size() > 1 && P[path.front()].size() > 1){
				if(stuck[path.front()] == true){
					vector<int> pathInP = P[path.front()];
					P[path.front()].pop_back();
					P[path.front()].pop_back();
					it->second.pop_back();
					it->second.pop_back();
				}
			}
		}
	}
	pair <int,set<Edge>> returnPair;
	returnPair = make_pair(A.size(),makePathMapToEdgeSet(A));
	return returnPair;
}

void printEdgeSet(set<Edge> set) {
	for (auto i = set.begin(); i != set.end(); ++i) {
		((Edge)(*i)).printEdge();
	}
	cout << endl;
}

void writeGraphToFile(string matchingFilename, string aSideFilename, string bSideFilename, set<Edge> M, BipartiteGraphStream bgs){
	ofstream myfile;
	myfile.open (matchingFilename);
	ofstream myfileA;
	myfileA.open (aSideFilename);
	ofstream myfileB;
	myfileB.open (bSideFilename);
	bgs.initStream();
	set<int> A = bgs.getASet();
	set<int> B = bgs.getBSet();
	stringstream ssAmount;
	ssAmount << bgs.numberOfNodes();
	string strAmount = ssAmount.str();
	string toFile = strAmount + "\n";
	while(bgs.hasNext()){
		Edge e = bgs.getNext();
		stringstream ssA;
		ssA << e.getVertexA();
		string strA = ssA.str();
		stringstream ssB;
		ssB << e.getVertexB();
		string strB = ssB.str();
		toFile += strA + "," + strB + ",";
		if(M.find(e) == M.end()){
			toFile += "1\n";
		}else{
			toFile += "2\n";
		}
		A.insert(e.getVertexA());
		B.insert(e.getVertexB());
	}
	myfile << toFile;
	myfile.close();
	for (set<int>::iterator it = A.begin(); it != A.end(); ++it){
		int vertex = *it;
		stringstream ssV;
		ssV << vertex;
		string vString = ssV.str();
		string toFile = vString + "\n";
		myfileA << toFile;
	}
	myfileA.close();
	for (set<int>::iterator it = B.begin(); it != B.end(); ++it){
		int vertex = *it;
		stringstream ssV;
		ssV << vertex;
		string vString = ssV.str();
		string toFile = vString + "\n";
		myfileB << toFile;
	}
	myfileB.close();
}

set<Edge> approximateMaximumMatching(BipartiteGraphStream bgs, float epsilon){
	//line 1 in algorithm 1
	int k = ceil(2/epsilon);
	cout <<  "K " << k << endl;
	//line 2 in algorithm 1
	set<Edge> M = inclusionMaximalMatching(bgs);
	float threshold = 1.0/(2.0*k*(k+2.0));
	cout <<  "Threshold " << threshold << endl;
	//line 3 in algorithm 1
	while(true){
		//line 4 in algorithm 1
		int c = M.size();
		cout <<  "Initial M size " << c << endl;
		//line 5 in algorithm 1
		pair <int,set<Edge>> augmentedPathsPair = disjointPaths(bgs, threshold, k, M);
		int numberOfPaths = augmentedPathsPair.first;
		cout <<  "Number of augmented paths found " << numberOfPaths << endl;
		set<Edge> augmentedPathsEdgeSet = augmentedPathsPair.second;
		//line 6 in algorithm 1
		for(set<Edge>::iterator itSet = augmentedPathsEdgeSet.begin(); itSet != augmentedPathsEdgeSet.end(); ++itSet){
			Edge edge = (Edge)*itSet;
			if(M.find(edge) == M.end()){
				M.insert(edge);
			}
			else{
				M.erase(M.find(edge));
			}
		}
		cout <<  "M size after symmetric difference with augmented paths " << M.size() << endl;
		if(!checkMatching(M)){
			cout <<  "MISTAKE" << endl;
		}
		
		//line 7 in algorithm 1
		if(numberOfPaths < c*threshold){
			writeGraphToFile("../wgPlot/matching.csv", "../wgPlot/aside.csv", "../wgPlot/bside.csv", M, bgs);
			break;
		}
	}
	//line 8 in algorithm 1
	return M;
}

int main(int argc, char **argv)
{
	int nrOfNodes = 100;
	cout <<  "Number of nodes in graph " << nrOfNodes << endl;
	BipartiteGraphStream bgs = BipartiteGraphStream(nrOfNodes);
	float epsilon = 0.1;
	approximateMaximumMatching(bgs, epsilon);
	return 0;
}